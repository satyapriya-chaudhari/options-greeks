import pandas as pd
import numpy as np
import dask.dataframe as dd

import mibian

# The data is stored in the directory 'data_modules'
path = "../OptionGreekSampleData/"

# Read the data
options_contract_definition = pd.read_csv(path + 'SPX_option_ric_info_20211012.csv',
                                          index_col=0)
options_contract_definition.index = pd.to_datetime(options_contract_definition.index)
options_contract_definition['expiry'] = pd.to_datetime(options_contract_definition['expiry']).dt.date
options_contract_definition['underlying_isin'] = 'US78378X1072'
options_contract_definition.rename(columns = {'option_ric':'ric',
                                              'expiry':'expiry_date',
                                              'underlying_ric':'symbol_ib'},
                                   inplace=True)
options_contract_definition['id'] = np.arange(len(options_contract_definition)) + 1

options_contract_definition = options_contract_definition[['id', 'underlying_isin',
                                                           'strike', 'expiry_date',
                                                           'option_type', 'ric', 'symbol_ib']]

PutStrike = options_contract_definition[options_contract_definition['ric'] == 'SPXv152143000.U']['strike'].iloc[0]
long_term_interest_rate = 0.008


def implied_volatility(options_greeks_daily):
    iv = mibian.BS([options_greeks_daily['UnderlyingPrice'],
                    PutStrike,
                    long_term_interest_rate,
                    int(str(options_greeks_daily['DaysToExpiration']).split()[0])],
                   putPrice=options_greeks_daily['PutPrice'])

    return (iv.impliedVolatility)


def greek_options(options_greeks_daily):
    greeks = mibian.BS([options_greeks_daily['UnderlyingPrice'],
                        PutStrike,
                        long_term_interest_rate,
                        int(str(options_greeks_daily['DaysToExpiration']).split()[0])],
                       volatility=options_greeks_daily['implied_volatility'])

    options_greeks_daily['delta'] = greeks.putDelta

    options_greeks_daily['gamma'] = greeks.gamma

    options_greeks_daily['theta'] = greeks.putTheta

    options_greeks_daily['vega'] = greeks.vega

    options_greeks_daily['rho'] = greeks.putRho

    return (options_greeks_daily)


def calculate_greek_options(options_greeks):
    options_greeks['implied_volatility'] = \
        options_greeks.apply(implied_volatility, axis=1, result_type='reduce')

    doptions_greeks = dd.from_pandas(options_greeks, npartitions=6)

    doptions_greeks = \
        doptions_greeks.map_partitions(
            lambda df: df.apply(
                (lambda row: greek_options(row)),
                axis=1)).compute()

    return (doptions_greeks)