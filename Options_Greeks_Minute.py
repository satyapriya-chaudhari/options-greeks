import pandas as pd
import sys
sys.path.append("..")
from Options_Greek_Utility_Functions import calculate_greek_options

def minute_data(options_contract_definition, minute_spx, minute_spx_option, options_greeks_minute):

    minute_spx['date_time'] = minute_spx.index
    minute_spx['isin'] = 'US78378X1072'
    minute_spx.rename(columns={'OPEN': 'open',
                               'HIGH': 'high',
                               'LOW': 'low',
                               'CLOSE': 'close',
                               'VOLUME': 'volume'}, inplace=True)
    minute_spx.drop(columns=['COUNT'], inplace=True)

    minute_spx = minute_spx[['isin', 'date_time', 'open',
                             'high', 'low', 'close']]
    minute_spx.loc[:, 'volume'] = 1

    minute_spx_option['date_time'] = minute_spx_option.index
    minute_spx_option['isin'] = 'US78378X1072'
    minute_spx_option.rename(columns={'OPEN': 'open',
                                      'HIGH': 'high',
                                      'LOW': 'low',
                                      'CLOSE': 'close',
                                      'VOLUME': 'volume'}, inplace=True)
    minute_spx_option.drop(columns=['COUNT'], inplace=True)

    minute_spx_option = minute_spx_option[['isin', 'date_time', 'open',
                                           'high', 'low', 'close', 'volume']]
    minute_spx_option.dropna(inplace=True)

    options_greeks_minute['date_time'] = options_greeks_minute.index

    options_greeks_minute['UnderlyingPrice'] = minute_spx['close']
    options_greeks_minute['DaysToExpiration'] = \
        (pd.to_datetime(
            options_contract_definition[options_contract_definition['ric'] ==
                                        'SPXv152143000.U']['expiry_date'][0]) - \
        options_greeks_minute.index).days + 1
    options_greeks_minute['PutPrice'] = minute_spx_option['close']

    options_greeks_minute = calculate_greek_options(options_greeks_minute)
    options_greeks_minute = options_greeks_minute.drop(columns=['UnderlyingPrice',
                                                                'DaysToExpiration',
                                                                'PutPrice'])

    options_greeks_minute = options_greeks_minute[['options_contract_id', 'date_time',
                                                   'delta', 'gamma', 'theta', 'vega',
                                                   'rho', 'implied_volatility']]

    options_greeks_minute = options_greeks_minute.dropna()

    return minute_spx, minute_spx_option, options_greeks_minute