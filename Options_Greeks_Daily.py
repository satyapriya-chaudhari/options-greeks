import pandas as pd
import sys
sys.path.append("..")
from Options_Greek_Utility_Functions import calculate_greek_options


def daily_data(options_contract_definition, daily_spx, daily_spx_option, options_greeks_daily):

    daily_spx['date_time'] = daily_spx.index
    daily_spx['isin'] = 'US78378X1072'
    daily_spx.rename(columns={'OPEN': 'open',
                              'HIGH': 'high',
                              'LOW': 'low',
                              'CLOSE': 'close',
                              'VOLUME': 'volume'}, inplace=True)
    # daily_spx.drop(columns=['COUNT'], inplace = True)
    daily_spx = daily_spx[['isin', 'date_time', 'open',
                           'high', 'low', 'close', 'volume']]

    daily_spx.loc[:, 'volume'] = 1


    daily_spx_option['date_time'] = daily_spx_option.index
    daily_spx_option['isin'] = 'US78378X1072'
    daily_spx_option.rename(columns={'OPEN': 'open',
                                     'HIGH': 'high',
                                     'LOW': 'low',
                                     'CLOSE': 'close',
                                     'VOLUME': 'volume'}, inplace=True)
    daily_spx_option.drop(columns=['COUNT'], inplace=True)

    daily_spx_option = daily_spx_option[['isin', 'date_time', 'open',
                                         'high', 'low', 'close', 'volume']]

    expiration_date = pd.to_datetime(
        options_contract_definition[options_contract_definition['ric'] == 'SPXv152143000.U']['expiry_date'][0])

    options_greeks_daily['date_time'] = options_greeks_daily.index
    options_greeks_daily['UnderlyingPrice'] = daily_spx['close']
    options_greeks_daily['DaysToExpiration'] = \
        (pd.to_datetime(expiration_date) - options_greeks_daily.index).days
    options_greeks_daily['PutPrice'] = daily_spx_option['close']

    options_greeks_daily = calculate_greek_options(options_greeks_daily)
    options_greeks_daily = options_greeks_daily.drop(columns=['UnderlyingPrice',
                                                              'DaysToExpiration',
                                                              'PutPrice'])

    options_greeks_daily = options_greeks_daily[['options_contract_id', 'date_time',
                                                 'delta', 'gamma', 'theta', 'vega',
                                                 'rho', 'implied_volatility']]

    return daily_spx, daily_spx_option, options_greeks_daily