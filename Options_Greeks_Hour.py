import pandas as pd
import sys
sys.path.append("..")
from Options_Greek_Utility_Functions import calculate_greek_options

def hourly_data(options_contract_definition, hour_spx, hour_spx_option, options_greeks_hour):

    hour_spx['date_time'] = hour_spx.index
    hour_spx['isin'] = 'US78378X1072'
    hour_spx.rename(columns={'OPEN': 'open',
                             'HIGH': 'high',
                             'LOW': 'low',
                             'CLOSE': 'close',
                             'VOLUME': 'volume'}, inplace=True)
    hour_spx.drop(columns=['COUNT'], inplace=True)
    hour_spx = hour_spx[['isin', 'date_time', 'open',
                         'high', 'low', 'close']]
    hour_spx.loc[:, 'volume'] = 1

    hour_spx.drop_duplicates(inplace=True)

    # Read the data
    hour_spx_option['date_time'] = hour_spx_option.index
    hour_spx_option['isin'] = 'US78378X1072'
    hour_spx_option.rename(columns={'OPEN': 'open',
                                    'HIGH': 'high',
                                    'LOW': 'low',
                                    'CLOSE': 'close',
                                    'VOLUME': 'volume'}, inplace=True)
    hour_spx_option.drop(columns=['COUNT'], inplace=True)

    hour_spx_option = hour_spx_option[['isin', 'date_time', 'open',
                                       'high', 'low', 'close', 'volume']]

    hour_spx_option.dropna(inplace=True)

    hour_spx_option.drop_duplicates(inplace=True)

    options_greeks_hour['date_time'] = options_greeks_hour.index

    options_greeks_hour['UnderlyingPrice'] = hour_spx['close']
    options_greeks_hour['DaysToExpiration'] = \
        (pd.to_datetime(
            options_contract_definition[options_contract_definition['ric'] ==
                                        'SPXv152143000.U']['expiry_date'][0]) - \
        options_greeks_hour.index).days + 1
    options_greeks_hour['PutPrice'] = hour_spx_option['close']

    options_greeks_hour = calculate_greek_options(options_greeks_hour)
    options_greeks_hour = options_greeks_hour.drop(columns=['UnderlyingPrice',
                                                            'DaysToExpiration',
                                                            'PutPrice'])

    options_greeks_hour = options_greeks_hour[['options_contract_id', 'date_time',
                                               'delta', 'gamma', 'theta', 'vega',
                                               'rho', 'implied_volatility']]

    options_greeks_hour = options_greeks_hour.dropna()

    return hour_spx, hour_spx_option, options_greeks_hour