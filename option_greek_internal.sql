CREATE TABLE `ib_option_greeks_data_daily` (
  `options_contract_id` int NOT NULL,
  `date_time` date NOT NULL,
  `delta` float DEFAULT NULL,
  `gamma` float DEFAULT NULL,
  `theta` float DEFAULT NULL,
  `vega` float DEFAULT NULL,
  `rho` float DEFAULT NULL,
  `implied_volatility` float DEFAULT NULL,
  PRIMARY KEY (`options_contract_id`,`date_time`),
  CONSTRAINT `FK_IB_OPTION_GREEKS_DATA_DAILY_ID` FOREIGN KEY (`options_contract_id`) REFERENCES `options_contract_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ib_option_greeks_data_hourly` (
  `options_contract_id` int NOT NULL,
  `date_time` datetime NOT NULL,
  `delta` float DEFAULT NULL,
  `gamma` float DEFAULT NULL,
  `theta` float DEFAULT NULL,
  `vega` float DEFAULT NULL,
  `rho` float DEFAULT NULL,
  `implied_volatility` float DEFAULT NULL,
  PRIMARY KEY (`options_contract_id`,`date_time`),
  CONSTRAINT `FK_IB_OPTION_GREEKS_DATA_HOURLY_ID` FOREIGN KEY (`options_contract_id`) REFERENCES `options_contract_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ib_option_greeks_data_1_minute` (
  `options_contract_id` int NOT NULL,
  `date_time` datetime NOT NULL,
  `delta` float DEFAULT NULL,
  `gamma` float DEFAULT NULL,
  `theta` float DEFAULT NULL,
  `vega` float DEFAULT NULL,
  `rho` float DEFAULT NULL,
  `implied_volatility` float DEFAULT NULL,
  PRIMARY KEY (`options_contract_id`,`date_time`),
  CONSTRAINT `FK_IB_OPTION_GREEKS_DATA_1_MINUTE_ID` FOREIGN KEY (`options_contract_id`) REFERENCES `options_contract_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `option_greeks_data_daily` (
  `options_contract_id` int NOT NULL,
  `date_time` date NOT NULL,
  `delta` float DEFAULT NULL,
  `gamma` float DEFAULT NULL,
  `theta` float DEFAULT NULL,
  `vega` float DEFAULT NULL,
  `rho` float DEFAULT NULL,
  `implied_volatility` float DEFAULT NULL,
  PRIMARY KEY (`options_contract_id`,`date_time`),
  CONSTRAINT `FK_OPTION_GREEKS_DATA_DAILY_ID` FOREIGN KEY (`options_contract_id`) REFERENCES `options_contract_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `option_greeks_data_hourly` (
  `options_contract_id` int NOT NULL,
  `date_time` datetime NOT NULL,
  `delta` float DEFAULT NULL,
  `gamma` float DEFAULT NULL,
  `theta` float DEFAULT NULL,
  `vega` float DEFAULT NULL,
  `rho` float DEFAULT NULL,
  `implied_volatility` float DEFAULT NULL,
  PRIMARY KEY (`options_contract_id`,`date_time`),
  CONSTRAINT `FK_OPTION_GREEKS_DATA_HOURLY_ID` FOREIGN KEY (`options_contract_id`) REFERENCES `options_contract_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `option_greeks_data_1_minute` (
  `options_contract_id` int NOT NULL,
  `date_time` datetime NOT NULL,
  `delta` float DEFAULT NULL,
  `gamma` float DEFAULT NULL,
  `theta` float DEFAULT NULL,
  `vega` float DEFAULT NULL,
  `rho` float DEFAULT NULL,
  `implied_volatility` float DEFAULT NULL,
  PRIMARY KEY (`options_contract_id`,`date_time`),
  CONSTRAINT `FK_OPTION_GREEKS_DATA_1_MINUTE_ID` FOREIGN KEY (`options_contract_id`) REFERENCES `options_contract_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `refinitiv_option_greeks_data_daily` (
  `options_contract_id` int NOT NULL,
  `date_time` date NOT NULL,
  `delta` float DEFAULT NULL,
  `gamma` float DEFAULT NULL,
  `theta` float DEFAULT NULL,
  `vega` float DEFAULT NULL,
  `rho` float DEFAULT NULL,
  `implied_volatility` float DEFAULT NULL,
  PRIMARY KEY (`options_contract_id`,`date_time`),
  CONSTRAINT `REFINITIV_OPTION_GREEKS_DATA_DAILY_ID` FOREIGN KEY (`options_contract_id`) REFERENCES `options_contract_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `refinitiv_option_greeks_data_hourly` (
  `options_contract_id` int NOT NULL,
  `date_time` datetime NOT NULL,
  `delta` float DEFAULT NULL,
  `gamma` float DEFAULT NULL,
  `theta` float DEFAULT NULL,
  `vega` float DEFAULT NULL,
  `rho` float DEFAULT NULL,
  `implied_volatility` float DEFAULT NULL,
  PRIMARY KEY (`options_contract_id`,`date_time`),
  CONSTRAINT `REFINITIV_OPTION_GREEKS_DATA_HOURLY_ID` FOREIGN KEY (`options_contract_id`) REFERENCES `options_contract_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `refinitiv_option_greeks_data_1_minute` (
  `options_contract_id` int NOT NULL,
  `date_time` datetime NOT NULL,
  `delta` float DEFAULT NULL,
  `gamma` float DEFAULT NULL,
  `theta` float DEFAULT NULL,
  `vega` float DEFAULT NULL,
  `rho` float DEFAULT NULL,
  `implied_volatility` float DEFAULT NULL,
  PRIMARY KEY (`options_contract_id`,`date_time`),
  CONSTRAINT `REFINITIV_OPTION_GREEKS_DATA_1_MINUTE_ID` FOREIGN KEY (`options_contract_id`) REFERENCES `options_contract_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `ticker_historical_data_adjusted_daily` (
  `isin` varchar(12) NOT NULL,
  `date_time` date NOT NULL,
  `open` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `volume` float DEFAULT NULL,
  PRIMARY KEY (`isin`,`date_time`),
  KEY `FK_TICKERS_ADJUSTED_HISTORICAL_DATA_DAILY_ISIN` (`isin`),
  CONSTRAINT `FK_TICKER_ADJUSTED_HISTORICAL_DATA_DAILY_ISIN` FOREIGN KEY (`isin`) REFERENCES `instrument_master` (`isin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ticker_historical_data_adjusted_hourly` (
  `isin` varchar(12) NOT NULL,
  `date_time` datetime NOT NULL,
  `open` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `volume` float DEFAULT NULL,
  PRIMARY KEY (`isin`,`date_time`),
  KEY `FK_TICKERS_ADJUSTED_HISTORICAL_DATA_HOURLY_ISIN` (`isin`),
  CONSTRAINT `FK_TICKER_ADJUSTED_HISTORICAL_DATA_HOURLY_ISIN` FOREIGN KEY (`isin`) REFERENCES `instrument_master` (`isin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ticker_historical_data_adjusted_1_minute` (
  `isin` varchar(12) NOT NULL,
  `date_time` date NOT NULL,
  `open` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `volume` float DEFAULT NULL,
  KEY `FK_TICKERS_ADJUSTED_HISTORICAL_DATA_1_MINUTE_ISIN_idx` (`isin`),
  CONSTRAINT `FK_TICKER_ADJUSTED_HISTORICAL_DATA_1_MINUTE_ISIN` FOREIGN KEY (`isin`) REFERENCES `instrument_master` (`isin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `ticker_historical_data_unadjusted_daily` (
  `isin` varchar(12) NOT NULL,
  `date_time` date NOT NULL,
  `open` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `volume` float DEFAULT NULL,
  PRIMARY KEY (`isin`,`date_time`),
  KEY `FK_TICKERS_UNADJUSTED_HISTORICAL_DATA_DAILY_ISIN` (`isin`),
  CONSTRAINT `FK_TICKER_UNADJUSTED_HISTORICAL_DATA_DAILY_ISIN` FOREIGN KEY (`isin`) REFERENCES `instrument_master` (`isin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `ticker_historical_data_unadjusted_hourly` (
  `isin` varchar(12) NOT NULL,
  `date_time` datetime NOT NULL,
  `open` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `volume` float DEFAULT NULL,
  PRIMARY KEY (`isin`,`date_time`),
  KEY `FK_TICKERS_UNADJUSTED_HISTORICAL_DATA_HOURLY_ISIN` (`isin`),
  CONSTRAINT `FK_TICKER_UNADJUSTED_HISTORICAL_DATA_HOURLY_ISIN` FOREIGN KEY (`isin`) REFERENCES `instrument_master` (`isin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `ticker_historical_data_unadjusted_1_minute` (
  `isin` varchar(12) NOT NULL,
  `date_time` datetime NOT NULL,
  `open` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `volume` float DEFAULT NULL,
  KEY `FK_TICKER_UNADJUSTED_HISTORICAL_DATA_1_MINUTE_ISIN_idx` (`isin`),
  CONSTRAINT `FK_TICKER_UNADJUSTED_HISTORICAL_DATA_1_MINUTE_ISIN` FOREIGN KEY (`isin`) REFERENCES `instrument_master` (`isin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `instrument_master` (
  `isin` varchar(12) NOT NULL COMMENT 'An International Securities Identification Number (ISIN) is a 12-digit alphanumeric code that uniquely identifies a specific security',
  `description` varchar(200) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL COMMENT 'Type of the Instrument - Foreign Key to the instrument_type master table',
  `ric` varchar(45) DEFAULT NULL COMMENT 'A Reuters instrument code, or RIC, is a ticker-like code used by Refinitiv to identify financial instruments and indices',
  `symbol_ib` varchar(45) DEFAULT NULL COMMENT 'Interactive Broker Symbol for the Instrument',
  PRIMARY KEY (`isin`),
  KEY `INSTRUMENT_TYPE_idx` (`type`),
  CONSTRAINT `FK_INSTRUMENT_MASTER_INSTRUMENT_TYPE` FOREIGN KEY (`type`) REFERENCES `instrument_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Master Table for Instuments (Index, Stocks along with their mapping corresponding codes with main brokers)';


CREATE TABLE `instrument_type` (
  `type` varchar(10) NOT NULL,
  `description` varchar(200) DEFAULT NULL COMMENT 'Instrument Type: \nSTK: Stock\nINX: Index\nFUT: Futures\nOPT: Options\netc.',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Master Table for various Instrument Types';


CREATE TABLE `options_contract_definition` (
  `id` int NOT NULL AUTO_INCREMENT,
  `underlying_isin` varchar(12) NOT NULL COMMENT 'ISIN Code for the Index',
  `strike` float DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `option_type` varchar(4) DEFAULT NULL,
  `ric` varchar(45) DEFAULT NULL,
  `symbol_ib` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_OPTIONS_CONTRACT` (`underlying_isin`,`strike`,`expiry_date`,`option_type`),
  CONSTRAINT `FK_OPTIONS_CONTRACT_ISIN` FOREIGN KEY (`underlying_isin`) REFERENCES `instrument_master` (`isin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;