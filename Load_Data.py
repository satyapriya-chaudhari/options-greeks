import pymysql

def load_instrument_type():
    # Enter the user name and password
    conn = pymysql.connect(database='aargo',
                           user=ENTERYOURUSERNAME,
-                          password=ENTERYOURPASSWORD)

    cursor = conn.cursor()
    cursor.execute("SELECT * FROM instrument_type;")
    insert_query = "INSERT INTO instrument_type VALUES ('OPT', 'This is an Options Instrument.');"
    cursor.execute(insert_query)

    conn.commit()
    conn.close()


def load_instrument_master():
    # Enter the user name and password
    conn = pymysql.connect(database='aargo',
                           user=ENTERYOURUSERNAME,
-                          password=ENTERYOURPASSWORD)

    cursor = conn.cursor()
    cursor.execute("SELECT * FROM instrument_master;")
    insert_query = \
        "INSERT INTO instrument_master VALUES ('US78378X1072', 'This is Instrument Description.', 'OPT', 'SPXJ152120000.U', 'SPX');"
    cursor.execute(insert_query)

    conn.commit()
    conn.close()


def load_data_to_sql(table_name, dataframe):

    # Enter the user name and password
    conn = pymysql.connect(database='aargo',
                           user=ENTERYOURUSERNAME,
-                          password=ENTERYOURPASSWORD)

    cursor = conn.cursor()
    cursor.execute("SELECT * FROM {};".format(table_name))

    insert_query = "INSERT INTO {} VALUES".format(table_name)

    for i in range(int(dataframe.shape[0])):
        insert_query += "("

        for j in range(int(dataframe.shape[1])):
            insert_query += "\'" + str(dataframe[dataframe.columns.values[j]][i]) + "\', "

        insert_query = insert_query[:-2] + "), "

    insert_query = insert_query[:-2] + ';'

    cursor.execute(insert_query)

    conn.commit()
    conn.close()