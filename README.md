**This program calculates the Option Greeks for a selected Options Contract**

It calculates Delta, Gamma, Vega, Rho, Theta and Implied Volatility and loads it to SQL Database.

---

## The steps are as below:

1. Create a Database (excute the below query):
	DROP DATABASE IF EXISTS aargo;
	CREATE DATABASE IF NOT EXISTS aargo;
	USE aargo;
2. Create the tables in the Database. The script for creating the tables can be found in the file `option_greeks_internal.sql`.
3. Clone this repo.
4. Goto **Load_Data.py**, enter your user name and password in `load_instrument_type`, `load_instrument_master` and `load_data_to_sql` functions.
5. Run **main.py**, change the file path stored in `path` as required. (Note: The CSV files are not available in the repo)

