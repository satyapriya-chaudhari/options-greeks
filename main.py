import pandas as pd
import numpy as np

import sys
sys.path.append("..")

from Options_Greeks_Daily import daily_data
from Options_Greeks_Hour import hourly_data
from Options_Greeks_Minute import minute_data
from Load_Data import load_data_to_sql, load_instrument_type, load_instrument_master

import warnings
warnings.filterwarnings("ignore")

# The data is stored in the directory 'OptionGreekSampleData'
path = "../OptionGreekSampleData/"

# Read the data
options_contract_definition = pd.read_csv(path + 'SPX_option_ric_info_20211012.csv',
                                          index_col=0)
options_contract_definition.index = pd.to_datetime(options_contract_definition.index)
options_contract_definition['expiry'] = pd.to_datetime(options_contract_definition['expiry']).dt.date
options_contract_definition['underlying_isin'] = 'US78378X1072'
options_contract_definition.rename(columns = {'option_ric':'ric',
                                              'expiry':'expiry_date',
                                              'underlying_ric':'symbol_ib'},
                                   inplace=True)
options_contract_definition['id'] = np.arange(len(options_contract_definition)) + 1

options_contract_definition = options_contract_definition[['id', 'underlying_isin',
                                                           'strike', 'expiry_date',
                                                           'option_type', 'ric', 'symbol_ib']]

PutStrike = options_contract_definition[options_contract_definition['ric'] == 'SPXv152143000.U']['strike'].iloc[0]
long_term_interest_rate = 0.008

# Read the data
daily_spx = pd.read_csv(path + 'daily_SPX_.SPX_ohlc_unadjusted_20211012.csv', index_col=0)
daily_spx.index = pd.to_datetime(daily_spx.index)

# Read the data
daily_spx_option = pd.read_csv(path + 'daily_SPX_option_SPXv152143000.U_ohlc_20211012.csv', index_col=0)
daily_spx_option.index = pd.to_datetime(daily_spx_option.index)

print("Calculating Options Greeks Daily...")
options_greeks_daily = pd.DataFrame(columns=['options_contract_id',
                                                 'UnderlyingPrice',
                                                 'DaysToExpiration',
                                                 'PutPrice'],
                                        index=daily_spx_option.index)

options_greeks_daily['options_contract_id'] = \
    options_contract_definition[options_contract_definition['ric'] == 'SPXv152143000.U']['id'].iloc[0]

daily_spx, daily_spx_option, options_greeks_daily = \
    daily_data(options_contract_definition, daily_spx, daily_spx_option, options_greeks_daily)

# Read the data
hour_spx = pd.read_csv(path + 'hour_SPX_.SPX_ohlc_unadjusted_20211012.csv', index_col=0)
hour_spx.index = pd.to_datetime(hour_spx.index)

# Read the data
hour_spx_option = pd.read_csv(path + 'hour_SPX_option_SPXv152143000.U_ohlc_20211012.csv', index_col=0)
hour_spx_option.index = pd.to_datetime(hour_spx_option.index)

print("Calculating Options Greeks Hourly...")
options_greeks_hour = pd.DataFrame(columns=['options_contract_id',
                                                'UnderlyingPrice',
                                                'DaysToExpiration',
                                                'PutPrice'],
                                       index=hour_spx_option.index)

options_greeks_hour['options_contract_id'] = \
    options_contract_definition[options_contract_definition['ric'] == 'SPXv152143000.U']['id'].iloc[0]

hour_spx, hour_spx_option, options_greeks_hour = \
    hourly_data(options_contract_definition, hour_spx, hour_spx_option, options_greeks_hour)

# Read the data
minute_spx = pd.read_csv(path + 'minute_SPX_.SPX_ohlc_unadjusted_20211012.csv', index_col=0)
minute_spx.index = pd.to_datetime(minute_spx.index)

# Read the data
minute_spx_option = pd.read_csv(path + 'minute_SPX_option_SPXv152143000.U_ohlc_20211012.csv', index_col=0)
minute_spx_option.index = pd.to_datetime(minute_spx_option.index)

print("Calculating Options Greeks Minute...")
options_greeks_minute = pd.DataFrame(columns=['options_contract_id',
                                                  'UnderlyingPrice',
                                                  'DaysToExpiration',
                                                  'PutPrice'],
                                         index=minute_spx_option.index)

options_greeks_minute['options_contract_id'] = \
    options_contract_definition[options_contract_definition['ric'] == 'SPXv152143000.U']['id'].iloc[0]

minute_spx, minute_spx_option, options_greeks_minute = \
    minute_data(options_contract_definition, minute_spx, minute_spx_option, options_greeks_minute)

refinitiv_options_greeks_daily = options_greeks_daily.copy()
# refinitiv_options_greeks_daily['options_contract_id'] = 'SPXv152143000.U'
refinitiv_options_greeks_hour = options_greeks_hour.copy()
# refinitiv_options_greeks_hour['options_contract_id'] = 'SPXv152143000.U'
refinitiv_options_greeks_minute = options_greeks_minute.copy()
# refinitiv_options_greeks_minute['options_contract_id'] = 'SPXv152143000.U'

ib_options_greeks_daily = options_greeks_daily.copy()
# ib_options_greeks_daily['options_contract_id'] = 'SPX'
ib_options_greeks_hour = options_greeks_hour.copy()
# ib_options_greeks_hour['options_contract_id'] = 'SPX'
ib_options_greeks_minute = options_greeks_minute.copy()
# ib_options_greeks_minute['options_contract_id'] = 'SPX'

data_map = {
    'options_contract_definition': options_contract_definition,
    'ib_option_greeks_data_1_minute': ib_options_greeks_minute,
    'ib_option_greeks_data_daily': ib_options_greeks_daily,
    'ib_option_greeks_data_hourly': ib_options_greeks_hour,
    'option_greeks_data_daily': options_greeks_daily,
    'option_greeks_data_hourly': options_greeks_hour,
    'option_greeks_data_1_minute': options_greeks_minute,
    'refinitiv_option_greeks_data_daily': refinitiv_options_greeks_daily,
    'refinitiv_option_greeks_data_hourly': refinitiv_options_greeks_hour,
    'refinitiv_option_greeks_data_1_minute': refinitiv_options_greeks_minute,
    'ticker_historical_data_adjusted_daily': daily_spx_option,
    'ticker_historical_data_unadjusted_daily': daily_spx,
    'ticker_historical_data_adjusted_1_minute': minute_spx_option,
    'ticker_historical_data_unadjusted_1_minute': minute_spx,
    'ticker_historical_data_adjusted_hourly': hour_spx_option,
    'ticker_historical_data_unadjusted_hourly': hour_spx
}

print("Loading instrument_type...")
load_instrument_type()
print("Loading instrument_master...")
load_instrument_master()

for data in data_map:
    print("Loading", data,"...")
    load_data_to_sql(data, data_map[data])
